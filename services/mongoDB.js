
const mongoose = require('mongoose');


// console.log('DBNAME : ', process.env.DBNAME);
// console.log('DBLOGIN : ', process.env.DBLOGIN);
// console.log('DBPW : ', process.env.DBPW);

const mongoDBurl = `mongodb+srv://${process.env.DBLOGIN}:${process.env.DBPW}@rfportfolio2021.ha6yx.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`

// console.log('url mongodb : ', mongoDBurl);

var options = {
  connectTimeoutMS: 5000,
  useNewUrlParser: true,
  useUnifiedTopology : true
 }
 mongoose.connect(mongoDBurl,
    options,        
    function(err) {
      if(err){
        console.log('!!! Err connecting to mongoDB : ', err);
      } else {
        console.log('~~~~~~~~~~~~~~~ connected to mongoDB ~~~~~~~~~~~~~~~~~~');
      }
    }
 );

 module.exports = mongoose;