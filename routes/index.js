var express = require('express');
var router = express.Router();

const experiencesModel = require('../models/Experiences');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Backend : /' });
});

/* GET home page. */
router.get('/travaux/:companyName', async function(req, res, next) {

    const compName = req.params.companyName

    experiencesModel.find({companyName:compName}, (err, docs) => {
      if(err){
        console.log('~~ err ~~ experiencesModel.find  : ', err);
        res.json({ success: false, err })
      }else{
        // console.log('~~ ok ~~ result of experiencesModel.find docs : ', docs)
        res.json({ success: true, experience : docs })
      }
    });

   }
);



module.exports = router;
