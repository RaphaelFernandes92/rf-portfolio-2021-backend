const mongoose = require('mongoose');
const { Schema } = mongoose;

const experiencesSchema = new Schema({
    bannerLink: String,
    companyName:  String, // String is shorthand for {type: String}
    description: String,
    imgLinks: [String],
    jobTitle:   String,
    period: String,
  });

  const experiencesModel = mongoose.model('experiences', experiencesSchema);

  module.exports = experiencesModel;

